<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/', function (Request $request, Response $response) {
    return $response->withRedirect('/admin');
});

$app->get('/admin', function (Request $request, Response $response) {
    return $response->withRedirect('/admin/orders');
});

//<editor-fold desc="Orders">
$app->get('/admin/orders', function (Request $request, Response $response) use ($view, $db) {
    $hallsDb = $db->query("SELECT idhall as id, hall as name FROM hallsofstudio",
        PDO::FETCH_ASSOC);
    $halls = [];
    foreach ($hallsDb as $hallDb){
        $halls[] = $hallDb;
    }
    $workersDb = $db->query("SELECT idworker as id, CONCAT(surname, name, patronymic) as name FROM workers",
        PDO::FETCH_ASSOC);
    $workers = [];
    foreach ($workersDb as $workerDb){
        $workers[] = $workerDb;
    }
    $customersDb = $db->query("SELECT idcustomer as id, CONCAT(surname, name, patronymic) as name FROM customers",
        PDO::FETCH_ASSOC);
    $customers = [];
    foreach ($customersDb as $customerDb){
        $customers[] = $customerDb;
    }

    $servicesDb = $db->query("SELECT idtype as id, service as name FROM typesofservice",
        PDO::FETCH_ASSOC);
    $services = [];
    foreach ($servicesDb as $serviceDb){
        $services[] = $serviceDb;
    }

    $data = [
        'pageNum' => 1,
        'halls' => $halls,
        'workers' => $workers,
        'customers' => $customers,
        'services' => $services
    ];
    return $view->render($response, 'orders.twig', $data);
});

$app->post('/admin/orders/list', function (Request $request, Response $response) use ($view, $db) {
    if(!$request->getParam('without_worker')){
        $rows = $db->query("SELECT idorder AS id, * FROM orders",
            PDO::FETCH_ASSOC);
    } else {
        $rows = $db->query("SELECT idorder AS id, * FROM orders WHERE idworker IS NULL",
            PDO::FETCH_ASSOC);
    }
    $result = [];
    foreach ($rows as $row) {
        $result[] = $row;
    }
    return $response->withJson([
        'Result' => 'OK',
        'Records' => $result,
        'TotalRecordCount' => count($result)
    ]);
});

$app->post('/admin/orders/create', function (Request $request, Response $response) use ($db) {
    $date = $request->getParam('dateisbooking');
    $time = $request->getParam('timeisbooking');
    $duration = $request->getParam('durationisbooking');
    $customerId = $request->getParam('idcustomer');
    $workerId = $request->getParam('idworker');
    $hallId = $request->getParam('idhall');
    $typeId = $request->getParam('idtype');
    $db->exec("INSERT INTO orders (dateisbooking, timeisbooking, durationisbooking, idcustomer, idworker, idhall, idtype) 
VALUES ('{$date}','{$time}', {$duration}, {$customerId}, NULLIF('{$workerId}', '')::INTEGER, {$hallId}, {$typeId});");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "dateisbooking" => $date,
            "timeisbooking" => $time,
            "durationisbooking" => $duration,
            "idcustomer" => $customerId,
            "idworker" => $workerId,
            "idhall" => $hallId,
            "idtype" => $typeId,
            "id" => $db->lastInsertId()
        ]
    ]);
});

$app->post('/admin/orders/update', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $date = $request->getParam('dateisbooking');
    $time = $request->getParam('timeisbooking');
    $duration = $request->getParam('durationisbooking');
    $customerId = $request->getParam('idcustomer');
    $workerId = $request->getParam('idworker');
    $hallId = $request->getParam('idhall');
    $typeId = $request->getParam('idtype');
    $db->exec("UPDATE orders 
              SET dateisbooking = '{$date}',
                  timeisbooking = '{$time}',
                  durationisbooking = '{$duration}',
                  idcustomer = '{$customerId}',
                  idworker = '{$workerId}',
                  idhall = '{$hallId}',
                  idtype = '{$typeId}'
              WHERE idorder = {$id};");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "dateisbooking" => $date,
            "timeisbooking" => $time,
            "durationisbooking" => $duration,
            "idcustomer" => $customerId,
            "idworker" => $workerId,
            "idhall" => $hallId,
            "idtype" => $typeId,
            "id" => $id
        ]
    ]);
});

$app->post('/admin/orders/delete', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $db->exec("DELETE FROM orders WHERE idorder = {$id};");

    return $response->withJson([
        'Result' => 'OK',
    ]);
});

//</editor-fold>

//<editor-fold desc="Users">
$app->get('/admin/users', function (Request $request, Response $response) use ($view) {
    $data = [
        'pageNum' => 2
    ];
    return $view->render($response, 'users.twig', $data);
});


$app->post('/admin/users/list', function (Request $request, Response $response) use ($view, $db) {
    $rows = $db->query("SELECT idcustomer AS id, * FROM customers",
        PDO::FETCH_ASSOC);
    $result = [];
    foreach ($rows as $row) {
        $result[] = $row;
    }
    return $response->withJson([
        'Result' => 'OK',
        'Records' => $result,
        'TotalRecordCount' => count($result)
    ]);
});

$app->post('/admin/users/create', function (Request $request, Response $response) use ($db) {
    $surname = $request->getParam('surname');
    $name = $request->getParam('name');
    $patronymic = $request->getParam('patronymic');
    $phone = $request->getParam('phone');
    $db->exec("INSERT INTO customers (surname, name, patronymic, phone) 
VALUES ('{$surname}','{$name}', '{$patronymic}', '{$phone}');");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "surname" => $surname,
            "name" => $name,
            "patronymic" => $patronymic,
            "phone" => $phone,
            "id" => $db->lastInsertId()
        ]
    ]);
});

$app->post('/admin/users/update', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $surname = $request->getParam('surname');
    $name = $request->getParam('name');
    $patronymic = $request->getParam('patronymic');
    $phone = $request->getParam('phone');
    $db->exec("UPDATE customers 
              SET surname = '{$surname}',
                  name = '{$name}',
                  patronymic = '{$patronymic}',
                  phone = '{$phone}'
              WHERE idworker = {$id};");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "surname" => $surname,
            "name" => $name,
            "patronymic" => $patronymic,
            "phone" => $phone,
            "id" => $id
        ]
    ]);
});

$app->post('/admin/users/delete', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $db->exec("DELETE FROM customers WHERE idcustomer = {$id};");

    return $response->withJson([
        'Result' => 'OK',
    ]);
});

//</editor-fold>

//<editor-fold desc="Photographers">
$app->get('/admin/photographers', function (Request $request, Response $response) use ($view) {
    $data = [
        'pageNum' => 3
    ];
    return $view->render($response, 'photographers.twig', $data);
});

$app->post('/admin/photographers/list', function (Request $request, Response $response) use ($view, $db) {
    $rows = $db->query("SELECT idworker AS id, * FROM workers",
        PDO::FETCH_ASSOC);
    $result = [];
    foreach ($rows as $row) {
        $result[] = $row;
    }
    return $response->withJson([
        'Result' => 'OK',
        'Records' => $result,
        'TotalRecordCount' => count($result)
    ]);
});

$app->post('/admin/photographers/create', function (Request $request, Response $response) use ($db) {
    $surname = $request->getParam('surname');
    $name = $request->getParam('name');
    $patronymic = $request->getParam('patronymic');
    $phone = $request->getParam('phone');
    $address = $request->getParam('address');
    $position = $request->getParam('position');
    $db->exec("INSERT INTO workers (surname, name, patronymic, phone, address, position) 
VALUES ('{$surname}','{$name}', '{$patronymic}', '{$phone}', '{$address}', '{$position}');");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "surname" => $surname,
            "name" => $name,
            "patronymic" => $patronymic,
            "phone" => $phone,
            "address" => $address,
            "position" => $position,
            "id" => $db->lastInsertId()
        ]
    ]);
});

$app->post('/admin/photographers/update', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $surname = $request->getParam('surname');
    $name = $request->getParam('name');
    $patronymic = $request->getParam('patronymic');
    $phone = $request->getParam('phone');
    $address = $request->getParam('address');
    $position = $request->getParam('position');
    $db->exec("UPDATE workers 
              SET surname = '{$surname}',
                  name = '{$name}',
                  patronymic = '{$patronymic}',
                  phone = '{$phone}',
                  address = '{$address}',
                  position = '{$position}'
              WHERE idworker = {$id};");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "surname" => $surname,
            "name" => $name,
            "patronymic" => $patronymic,
            "phone" => $phone,
            "address" => $address,
            "position" => $position,
            "id" => $id
        ]
    ]);
});

$app->post('/admin/photographers/delete', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $db->exec("DELETE FROM workers WHERE idworker = {$id};");

    return $response->withJson([
        'Result' => 'OK',
    ]);
});
//</editor-fold>

//<editor-fold desc="Survey types">
$app->get('/admin/survey_types', function (Request $request, Response $response) use ($view, $db) {
    $data = [
        'pageNum' => 4,
    ];
    return $view->render($response, 'survey_types.twig', $data);
});

$app->post('/admin/survey_types/list', function (Request $request, Response $response) use ($view, $db) {
    if($request->getParam('most_popular')){
        $rows = $db->query("SELECT DISTINCT durationisbooking, orders.idtype as id, service, priceofservice as price, 
orders.durationisbooking * typesofservice.priceofservice as aboveaveragesales
FROM orders, typesofservice
WHERE  orders.idtype = typesofservice.idtype
AND orders.durationisbooking * typesofservice.priceofservice >
(SELECT avg (orders.durationisbooking * typesofservice.priceofservice)
FROM orders, typesofservice
WHERE  orders.idtype = typesofservice.idtype)
LIMIT 1", PDO::FETCH_ASSOC);
    } else {
        $rows = $db->query("SELECT idtype AS id, service, priceofservice AS price FROM typesofservice",
            PDO::FETCH_ASSOC);
    }
    $result = [];
    foreach ($rows as $row) {
        $result[] = $row;
    }
    return $response->withJson([
        'Result' => 'OK',
        'Records' => $result,
        'TotalRecordCount' => count($result)
    ]);
});

$app->post('/admin/survey_types/create', function (Request $request, Response $response) use ($db) {
    $service = $request->getParam('service');
    $price = $request->getParam('price');
    $db->exec("INSERT INTO typesofservice (service, priceofservice) VALUES ('{$service}',{$price});");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "service" => $service,
            "price" => $price,
            "id" => $db->lastInsertId()
        ]
    ]);
});

$app->post('/admin/survey_types/update', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $service = $request->getParam('service');
    $price = $request->getParam('price');
    $db->exec("UPDATE typesofservice SET service = '{$service}', priceofservice = {$price} WHERE idtype = {$id};");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "service" => $service,
            "price" => $price,
            "id" => $db->lastInsertId()
        ]
    ]);
});

$app->post('/admin/survey_types/delete', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $db->exec("DELETE FROM typesofservice WHERE idtype = {$id};");

    return $response->withJson([
        'Result' => 'OK',
    ]);
});
//</editor-fold>

//<editor-fold desc="Room types">
$app->get('/admin/room_types', function (Request $request, Response $response) use ($view) {
    $data = [
        'pageNum' => 5
    ];
    return $view->render($response, 'room_types.twig', $data);
});

$app->post('/admin/room_types/list', function (Request $request, Response $response) use ($view, $db) {

    if($request->getParam('most_popular')){
        $rows = $db->query("SELECT DISTINCT orders.idhall as id, hall, rentalprice as price, 
orders.durationisbooking * hallsofstudio.rentalprice as aboveaveragesales
FROM orders, hallsofstudio
WHERE  orders.idhall=hallsofstudio.idhall
AND orders.durationisbooking * hallsofstudio.rentalprice >
(SELECT avg (orders.durationisbooking * hallsofstudio.rentalprice)
FROM orders, hallsofstudio
WHERE  orders.idhall=hallsofstudio.idhall)
LIMIT 1;", PDO::FETCH_ASSOC);
    } else {
        $rows = $db->query("SELECT idhall AS id, hall, rentalprice AS price FROM hallsofstudio",
            PDO::FETCH_ASSOC);
    }

    $result = [];
    foreach ($rows as $row) {
        $result[] = $row;
    }
    return $response->withJson([
        'Result' => 'OK',
        'Records' => $result,
        'TotalRecordCount' => count($result)
    ]);
});

$app->post('/admin/room_types/create', function (Request $request, Response $response) use ($db) {
    $hall = $request->getParam('hall');
    $price = $request->getParam('price');
    $db->exec("INSERT INTO hallsofstudio (hall, rentalprice) VALUES ('{$hall}',{$price});");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "hall" => $hall,
            "price" => $price,
            "id" => $db->lastInsertId()
        ]
    ]);
});

$app->post('/admin/room_types/update', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $hall = $request->getParam('hall');
    $price = $request->getParam('price');
    $db->exec("UPDATE hallsofstudio SET hall = '{$hall}', rentalprice = {$price} WHERE idhall = {$id};");

    return $response->withJson([
        'Result' => 'OK',
        'Record' => [
            "hall" => $hall,
            "price" => $price,
            "id" => $db->lastInsertId()
        ]
    ]);
});

$app->post('/admin/room_types/delete', function (Request $request, Response $response) use ($db) {
    $id = $request->getParam('id');
    $db->exec("DELETE FROM hallsofstudio WHERE idhall = {$id};");

    return $response->withJson([
        'Result' => 'OK',
    ]);
});
//</editor-fold>
