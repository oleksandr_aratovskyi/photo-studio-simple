<?php

setlocale(LC_ALL, 'ru_RU');
date_default_timezone_set('GMT');

require_once __DIR__ . '/../vendor/autoload.php';

try {
    $dot_env = new Dotenv\Dotenv(__DIR__ . "/../");
    $dot_env->load();
} catch(Exception $e){

}

$config = [
    'settings' => [
        'displayErrorDetails' => true,
        'debug' => true
    ]
];

$app = new \Slim\App($config);

$container = $app->getContainer();
$view = new \Slim\Views\Twig("../templates");
/** @var \Slim\Http\Request $request */
$request = $container['request'];
/** @var \Slim\Http\Uri $uri */
$uri = $request->getUri();
$basePath = rtrim(str_ireplace('index.php', '', $uri->getBasePath()), '/');
$view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

$host = getenv('DB_HOST');
$dbName = getenv('DB_NAME');
$dbUser = getenv('DB_USER');
$dbPassword = getenv('DB_PASSWORD');
$dsn = "pgsql:host={$host};port=5432;dbname={$dbName};user={$dbUser};password={$dbPassword}";
$db = new PDO($dsn);

require_once __DIR__ . '/../routes.php';

if (php_sapi_name() != "cli") {
    $app->run();
}